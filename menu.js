module.exports = function(rh){
	rh.app.get('/',function(req, res){
		var session = false;
		var username = "";
		if(req.session.user){
			username = req.session.user;
			session = true;
		}
		var render = rh.swig.renderFile('menu.html', {
	    	username: username,
	    	session: session
		});
		res.send(render);
	});
}
module.exports = function(rh){
	rh.app.get('/chat',function(req,res){
		if(!req.session.user){
			res.redirect('login');
			return;
		}
		username = req.session.user;

		rh.db.run('CREATE TABLE IF NOT EXISTS rooms (name TEXT, creator TEXT, qNum INT, maxPlayers INT, time INT)',function(err){
			if(err){
				console.log(err);
				res.send("error in server");
				return;
			}
			var rooms = [];
			rh.db.all("SELECT name FROM rooms",function(err, rows){
				if(rows.length == 0){
					rooms = false;
				}
				else{
					rows.forEach(function(row){
						rooms.push(row.name);
					});
				}
				var render = rh.swig.renderFile('roomSelect.html', {
		    		rooms: rooms
				});
				res.send(render);
			});
		});
	});

	rh.app.post('/chat',function(req,res){
		var username = req.session.user;
		var room_name = req.body.roomName;
		var qNum = req.body.qNum;
		var maxPlayers = req.body.maxPlayers;
		var time = req.body.time;

		rh.db.run('CREATE TABLE IF NOT EXISTS rooms (name TEXT, creator TEXT, qNum INT, maxPlayers INT, time INT)',function(err){
			var query = rh.db.prepare("INSERT INTO rooms VALUES (?, ?, ?, ?, ?)");
			query.run(room_name, username, qNum, maxPlayers, time,  function(err){
				if(err){
					res.send("error while creating room");
					console.log(err);
					return;
				}
				rh.waitingRoom.emit('new room', room_name);
				res.redirect('/room?name='+room_name);
			});
		});
	});

}
module.exports = function(rh){
	rh.app.get('/register',function(req, res){
		var render = rh.swig.renderFile('register.html', {
		    	error: ""
		});
		res.send(render);
	});

	rh.app.post('/register', function(req,res){
		var username = req.body.username;
		var password = req.body.password;
		var email = req.body.email;
		rh.db.run('CREATE TABLE IF NOT EXISTS users (username TEXT UNIQUE, password TEXT, numOfGames INT, numOfAnswers INT, numOfRight INT, avgTime REAL)',function(err){
			var checkExist = rh.db.prepare('SELECT username FROM users WHERE username = ?');
			checkExist.all(username, function(err,rows){
				if(err){
					res.send("error in server");
					return;
				}
				if(rows.length != 0){
					var render = rh.swig.renderFile('register.html', {
	    				error: "username already taken"
					});
					res.send(render);
					return;
				}
				var query = rh.db.prepare("INSERT INTO users VALUES (?, ?, 0, 0, 0, 0.0)");
				query.run(username, rh.passHash.generate(password),function(err){
					res.redirect("/login");
				});
			});
		});
	});
}
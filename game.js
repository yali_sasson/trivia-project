module.exports.createGame = function(qNumber, db, callback){
	db.all('SELECT * FROM questions', function(err, data) {
		if(err) {
			callback(err);
		}
		callback(null, data.sort(x => Math.random() - 0.5).slice(0, qNumber));
	});
}

module.exports.checkAns = function(db, question, answer, callback){
	var query = db.prepare('SELECT rightAns FROM questions WHERE q=?');
	query.all(question, function(err, data) {
		if(err) {
			callback(err);
		}
		callback(null, data[0].rightAns == answer);
	});
}

module.exports.updateUserStats = function(db, username, right, ansTime){
	var query = db.prepare('UPDATE users SET numOfAnswers = numOfAnswers+1 WHERE username=?');
	query.run(username, function(err){
		var updateTime = db.prepare('SELECT numOfAnswers, avgTime FROM users WHERE username=?');
		updateTime.all(username,function(err, data){
			if(err){
				console.log(err);
				return;
			}
			var average = data[0].avgTime + ((ansTime - data[0].avgTime) / data[0].numOfAnswers);
			db.prepare('UPDATE users SET avgTime = ? WHERE username=?').run(average,username,function(err){
				if(err){
					console.log(err);
				}
			});
		});
	});
	if(right){
		db.prepare('UPDATE users SET numOfRight = numOfRight+1 WHERE username=?').run(username,function(err){
			if(err){
				console.log(err);
			}
		});
	}	
}


module.exports.finishGame = function(db, username, score){
	db.prepare('UPDATE users SET numOfGames = numOfGames+1 WHERE username=?').run(username,function(err){
		if(err){
			console.log(err);
		}
	});
	db.run('CREATE TABLE IF NOT EXISTS topScores (username INT, score INT)',function(err){
		if(err){
			console.log(err);
		}
		db.prepare('SELECT * FROM topScores WHERE username=?').all(function(err,data){
			if(err){
				console.log(err);
				return;
			}
			if(data.length == 0){
				db.prepare('INSERT INTO topScores VALUES (?,?)').run(username,score,function(err){
					if(err){
						console.log(err);
					}
				});
				return;
			}
			if(score > data[0].score){
				db.prepare('UPDATE topScores SET score = ? WHERE username = ?').run(score,username,function(err){
					if(err){
						console.log(err);
					}
				});
			}
		});
	});
}
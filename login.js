module.exports = function(rh){
	rh.app.get("/login",function(req, res){
		var render = rh.swig.renderFile('login.html', {
	    	error: false
		});
		res.send(render);
	});

	rh.app.post("/login", function(req, res){
		var username = req.body.username;
		var password = req.body.password;
		rh.db.run("CREATE TABLE IF NOT EXISTS users (username TEXT UNIQUE, password TEXT, numOfGames INT, numOfAnswers INT, numOfRight INT, avgTime REAL)",function(err){
			var query = rh.db.prepare("SELECT username, password FROM users WHERE username = ?");
			query.each(username,function(err, row){
				if(rh.passHash.verify(password,row.password)){
					req.session.user = username;
					res.redirect('/');
					return;
				}
				else{
					var render = rh.swig.renderFile('login.html', {
	    				error: "wrong password!"
					});
					res.send(render);
					return;
				}
				if(err){
					res.send("error in server");
					return;
				}
			},function(err, rows) {
	  			if (rows == 0) {
	    			var render = rh.swig.renderFile('login.html', {
	    				error: "wrong username!"
					});
					res.send(render);
					return;
	    		}
	    	});
		});
	});
}
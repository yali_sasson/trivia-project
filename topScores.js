module.exports = function(rh){
	rh.app.get('/top',function(req,res){
		rh.db.all('SELECT * FROM topScores ORDER BY score DESC LIMIT 3',function(err,data){
			if(err){
				console.log(err);
			}
			var scores = [null , null, null]
			for(i = 0; i < 3; i++){
				if(data[i]){
					scores[i] = data[i].username + " : " + data[i].score;
				}
			}
			var render = rh.swig.renderFile('topscores.html', {
	    		scores: scores
			});
			res.send(render);
		});
	});
}
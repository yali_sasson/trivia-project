module.exports = function(rh){
	rh.app.get('/logout', function(req, res){
		if(req.session.user){
			req.session.user = null;
			res.redirect('/');
		}
		else{
			res.redirect('/');
		}
	});
}
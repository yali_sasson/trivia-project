module.exports = function(rh){
	rh.app.get('/profile',function(req, res){
		if(!req.session.user){
			res.redirect("/login");
			return;
		}
		var username = req.session.user;
		var query = rh.db.prepare("SELECT * FROM users WHERE username = ?");
		query.all(username,function(err, data){
			if(err){
				res.send("error in server!");
				console.log(err);
				return;
			}
			else{
				var render = rh.swig.renderFile('profile.html', {
	    			username: username,
	    			numOfGames : data[0].numOfGames,
	    			numOfRight : data[0].numOfRight,
	    			numOfWrong : data[0].numOfAnswers - data[0].numOfRight,
	    			avgTime : data[0].avgTime.toFixed(2)
				});
				res.send(render);
			}
		});
	});

}
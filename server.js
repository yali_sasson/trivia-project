var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();;
var passHash = require('password-hash');
var installFive = require("./five.js");
var fs = require('fs');
var _ = require('lodash');
var session = require('express-session');
var swig = require('swig');
var server = app.listen(8080,'0.0.0.0');
var io = require('socket.io').listen(server);


var db = new sqlite3.Database('chatDB.db');

app.use(bodyParser());
app.use(session({secret: 'BENHAMELECH2'}));
var rooms = new Array();
var gameRooms = new Array();

var waitingRoom = io.of("/chat");
var chatting = io.of("/room");
var game = io.of("/game");

var rh = {
	app : app,
	db : db,
	swig : swig,
	_ : _,
	io: io,
	passHash: passHash,
	waitingRoom : waitingRoom
};


var game = require('./game.js');

require('./menu.js')(rh);
require('./register.js')(rh);
require('./login.js')(rh);
require('./profile.js')(rh);
require('./roomSelect.js')(rh);
require('./logout.js')(rh);
require('./topScores.js')(rh);





var username;
var roomName;

app.get('/room', function(req,res){
	if(!req.session.user){
		res.redirect('login');
		return;
	}
	username = req.session.user;
	if(!req.query.name){
		res.redirect('/chat');
		return;
	}
	roomName = req.query.name;
	db.run("CREATE TABLE IF NOT EXISTS rooms (name TEXT, creator TEXT, qNum INT, maxPlayers INT, time INT)", function(err){
		if(err){
			console.log(err);
			res.send("error in server");
			return;
		}
		var query = db.prepare("SELECT name, creator FROM rooms WHERE name=?");
		query.all(roomName,function(err,rows){
			if(err){
				console.log(err);
				res.send("error in server");
				return;
			}
			if(rows.length == 0){
				res.redirect('/chat');
				return;
			}

			var ImAdmin = (rows[0].creator == req.session.user);

			var render = swig.renderFile('chat.html', {
    			room: rows[0].name,
    			admin : rows[0].creator,
    			ImAdmin : ImAdmin
			});
			res.send(render);
		});
	});
});


chatting.on('connection', function (socket) {
	var socketUser = username;
	console.log("got connection from " + socketUser);
	var socketRoom = roomName;
	var gameRoom;

	var qNum;
	var time;

	var questionCounter = 0;
	var answerCounter = 0;
	var score = 0;
	var currentTime;
	
	db.prepare('SELECT maxPlayers FROM rooms WHERE name =?').all(socketRoom, function(err,data){
		if(err){
			console.log(err);
			return;
		}

		if(data.length == 0){
			return;
		}

		if(!rooms[socketRoom]){
			rooms[socketRoom] = [];
		}
		if(!(data[0].maxPlayers <= rooms[socketRoom].length)){
			socket.join(socketRoom);
			chatting.in(socketRoom).emit('join', socketUser);
			rooms[socketRoom].push(socketUser);
			chatting.in(socketRoom).emit('online list', rooms[socketRoom]);
		}
		else{
			socket.emit('full');
		}
	});

	db.prepare('SELECT time, qNum FROM rooms WHERE name=?').all(socketRoom,function(err,data){
		if(err){
			console.log(err);
		}
		qNum = data[0].qNum;
		time = data[0].time;
		console.log('time set to ' + time);
	});

	socket.on('new message', function (data) {
    	chatting.in(socketRoom).emit('chat', "<b>" + socketUser +": </b>" + data);
  	});

  	socket.on('start game',function(){
  		console.log('starting new game');
  		gameRooms[socketRoom] = rooms[socketRoom];
  		game.createGame(qNum, db, function(err, data){
  			if(err){
  				console.log('error');
  				return;
  			}
  			gameRoom = data;
  			var answers = [gameRoom[0].rightAns, gameRoom[0].ans2, gameRoom[0].ans3, gameRoom[0].ans4];
  			answers = _.shuffle(answers);
  			var str = 
	  			'<center> <div id="game"> \
				<span id="q">' + gameRoom[0].q + '</span><br>\
				<button onclick="buttonClicked(this)" id="b1">' + answers[0] +'</button><br>\
				<button onclick="buttonClicked(this)" id="b2">' + answers[1] +'</button><br>\
				<button onclick="buttonClicked(this)" id="b3">' + answers[2] +'</button><br>\
				<button onclick="buttonClicked(this)" id="b4">' + answers[3] +'</button><br>\
				<span style="position:absolute;bottom:5" id="score"></span>\
				</div></center>';
			chatting.in(socketRoom).emit('start game', {
				html : str,
				gameRoom : gameRoom
			});
			checkTimeOut(socketRoom,time,gameRoom[0].q);

  		});
  		
  	});

  	socket.on('startConfirm',function(data){
  		gameRoom = data;
  		questionCounter++;
  		var d = new Date;
  		currentTime = d.getTime();
  	});

  	socket.on('iAnswered',function(){
  		console.log(questionCounter);
  		if(questionCounter == qNum){//replace with db value
  			chatting.in(socketRoom).emit('finish',{	
  				username: socketUser,
  				score : score
  			});
  			game.finishGame(db, socketUser, score);
  			return;
  		}
  		if(answerCounter == rooms[socketRoom].length){
  			var answers = [gameRoom[questionCounter].rightAns, gameRoom[questionCounter].ans2, gameRoom[questionCounter].ans3, gameRoom[questionCounter].ans4];
  			answers = _.shuffle(answers);
  			var q = gameRoom[questionCounter].q;
  			chatting.in(socketRoom).emit('question',{
  				q : q,
  				answers: answers
  			});
  			checkTimeOut(socketRoom,time,q);
  			chatting.in(socketRoom).emit('resetAns');
  		}
  	});

  	socket.on('answer',function(data){
  		if(data.answer == 'time out'){
  			socket.emit('answer',{
	  			result :false,
	  			score: score + '/' + qNum
	  		});
	  		game.updateUserStats(db,socketUser,false,time);
	  		return;
  		}
  		game.checkAns(db,gameRoom[questionCounter-1].q,data.answer,function(err,result){
  			if(err){
  				console.log(err);
  			}
  			if(result) score++;
  			socket.emit('answer',{
	  			result :result,
	  			score: score + '/' + qNum
	  		});
	  		var d = new Date();
	  		game.updateUserStats(db,socketUser,result,((d.getTime() - currentTime)/1000));
  		});
  		chatting.in(socketRoom).emit('addAnswer');
  	});

  	socket.on('addAnswer',function(){
  		answerCounter++;
  	});

  	socket.on('resetAns',function(){
  		answerCounter = 0;
  		questionCounter++;
  		var d = new Date()
  		currentTime = d.getTime();
  	})

  	socket.once('disconnect',function(){
  		chatting.in(socketRoom).emit('left', socketUser);
  		if(rooms[socketRoom]){
  			rooms[socketRoom].splice(rooms[socketRoom].indexOf(socketUser), 1);
  		}
  		chatting.in(socketRoom).emit('online list', rooms[socketRoom]);
  		console.log(rooms[socketRoom]);
  		console.log(socketUser + " disconnected");
		var query = db.prepare('SELECT name FROM rooms WHERE creator = ?');
		query.all(socketUser, function(err, rows){
	 		if(rows.length != 0){
	 			rows.forEach(function(row){
	 				chatting.in(row.name).emit('close');
	 				delete rooms[row.name]; 
	 			});
	 			var deleteQuery = db.prepare("DELETE FROM rooms WHERE creator = ?");
	 			deleteQuery.run(socketUser);
	 		}
		});
	});
});

function checkTimeOut(room, time, q){
	setTimeout(function(){
		console.log("question: " + q +" timed out after " + time + "seconds")
		chatting.in(room).emit('timeOut', q);
	}, time*1000);
}
